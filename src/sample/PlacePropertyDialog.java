package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class PlacePropertyDialog extends Stage {
    public static final int ICON_INFO = 0;
    public static final int ICON_ERROR = 1;
    public static Place p;
    public static Scene scene;

    public PlacePropertyDialog(Stage owner, Place p, int type) {
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);

        this.p = p;

        try {
            Parent root= FXMLLoader.load(getClass().getResource("PlacePropertyDialog.fxml"));
            scene= new Scene(root);
            scene.setFill(Color.WHEAT);
            setScene(scene);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
