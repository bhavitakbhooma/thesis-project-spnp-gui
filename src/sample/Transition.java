package sample;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Transition {
    public static int id;
    public String label;
    private double rate;
    private Place inputPlace;
    private Place outputPlace;
    public  double x;
    public  double y;
    int counter;
    public Transition(int id, String label,double x, double y)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.label=label;
    }

}
