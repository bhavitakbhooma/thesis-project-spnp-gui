package sample;

import java.util.ArrayList;



public class Place {
    public int id;
    public String label;
    public int tokens;
    //    private int noOfTokens;
    private Transition inputTransition;
    private Transition outputTransition;
    public  double x;
    public  double y;

    public ArrayList<String> vInput = new ArrayList<>();
    public ArrayList<String> vOutput = new ArrayList<>();

    //Place Constructor
    public Place(int id, double x, double y, String label , ArrayList<String> vInput, ArrayList<String> vOutput)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.label = label;
        this.vInput = vInput;
        this.vOutput = vOutput;

    }

//    private void setNoOfTokens(int tokens)
//    {
//        this.noOfTokens=tokens;
//    }
}
