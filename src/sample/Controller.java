package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.scene.transform.Translate;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class Controller {
    public static int count=0;
    public static  ArrayList<Stack> stacks = new ArrayList<>();
    public static  ArrayList<Stack> tempStacks = new ArrayList<>();


    private ArrayList<Place> places = new ArrayList<>();
    private int circleCounter = 1;
    String clickStatus = "C";
    private  double  circleX = 210;
    private  double  circleY = 40;

    private ArrayList<Transition> t_transition = new ArrayList<>();
    private ArrayList<Transition> imm_transition = new ArrayList<>();
    private int rectCounter = 1;
    public ArrayList<String> child = new ArrayList<>();

    private ArrayList<Arcs> arcs = new ArrayList<>();
    private int a_count=0;
    String vIn="";
    String vOut="";

    public String label;
    public ArrayList<String> vInput = new ArrayList<>();
    public ArrayList<String> vOutput = new ArrayList<>();


    public static String cLabel="";
    public static Text txt;
    public static Place ppp;
    public static Transition ttt;

    @FXML
    private VBox vbox;

    @FXML
    private MenuItem newModelButton;

    @FXML
    private MenuItem saveButton;

    @FXML
    private MenuItem loadButton;

    @FXML
    private Button placeButton;

    @FXML
    private Button arcButton;

    @FXML
    private Button transitionButton;

    @FXML
    private Button analyzeButton;

    @FXML
    private Button clearButton;

    @FXML
    private Button deleteButton;

    @FXML
    private GridPane grids;

    @FXML
    private MenuItem ctrlz;

    @FXML
    private MenuItem ctrly;

    @FXML
    public  BorderPane root;




    @FXML
    void handleLoadClicked(ActionEvent event) {

    }

    @FXML
    void handleNewModelClicked(ActionEvent event) {

    }

    @FXML
    void handleSaveClicked(ActionEvent event) {
        saveModel();
    }

    @FXML
    void onAnalyzeButtonClicked(ActionEvent event) {
        System.out.println("Clicked Analyse Button");
        createCFile();
    }

    private void createCFile() {
        DirectoryChooser dir_chooser = new DirectoryChooser();
        File file = dir_chooser.showDialog(null);
        if (file != null) {
            System.out.println(file.getAbsolutePath() + "  selected");
            File f = new File(file.getAbsolutePath(),"test.c");
            try
            {
                f.createNewFile();
                FileOutputStream is = new FileOutputStream(f);
                OutputStreamWriter osw = new OutputStreamWriter(is);
                Writer w = new BufferedWriter(osw);
                if(!places.isEmpty())
                {
                    for(int i=0;i<places.size();i++)
                    {
                        Place p = places.get(i);
                        w.write("Place Name : "+p.label+"\n");
                        w.write("X , Y  : "+p.x+" , "+p.y+"\n");
                        w.write("\n");
                        int obj = 0;
                        ArrayList<String> dest =new ArrayList<>();
                        for(Arcs l :arcs)
                        {

                            System.out.println("vIn : "+l.v_in);
                            System.out.println("vOut : "+l.v_out+"\n\n");
                            if(l.v_out.equals("P"+(i+1)))
                            {
                                dest.add(l.v_in);
                                w.write("Dest : "+l.v_in+" , Arcs : arcs"+l.id+"\n");
                                obj=obj+1;
                            }

                            if(l.v_in.equals("P"+(i+1)))
                            {
                                w.write("vInput from : "+l.v_out+" by Arcs : arcs"+l.id+"\n");
                            }
                        }
                        w.write("Number of connected object : "+obj+"\n");
                        w.write("\n\n\n");
                    }

                }
                w.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void onArcButtonClicked(ActionEvent event) {
        System.out.println("Clicked Arc Button");
        clickStatus="A";
        child.add("A");
        createArc();

    }

    public void createArc() {
        Arc arc = new Arc();
        root.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if(clickStatus=="A")
                {
                    switch (event.getButton()) {
                        case PRIMARY:

                            arc.setEndX(event.getX());
                            arc.setEndY(event.getY());
                            break;
                        case SECONDARY:

                            arc.setStartX(event.getX());
                            arc.setStartY(event.getY());
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        stacks.add(new Stack("a",null,arc));
        root.getChildren().add(arc);
        a_count++;
    }


    @FXML
    void onClearButtonClicked(ActionEvent event) {
        System.out.println("Clicked Clear Button");
        root.getChildren().clear();
        circleCounter=1;
        rectCounter=1;
        places.clear();
        t_transition.clear();
        imm_transition.clear();
        arcs.clear();
        a_count=0;

    }

    @FXML
    void onDeleteButtonClicked(ActionEvent event) {
        clickStatus="D";

    }

    @FXML
    void onPlaceButtonClicked(ActionEvent event) {
        System.out.println("Clicked Place Button");
        clickStatus="C";
        String label = "P"+circleCounter;
        places.add(new Place(circleCounter, circleX, circleY,label,vInput,vOutput));

        Place p = places.get(places.size()-1);
        Random random = new Random();
        int max =  random.nextInt(300);
        int min =  random.nextInt(200);
        circleX = Math.random()*(max-min+1)+min;
        circleY = Math.random()*(max-min+1)+min;

//        child.add("C");
//        System.out.println(child.size());
//        for(String s:child)
//        {
//            System.out.println(s+"\n");
//        }

        createPlace(p,circleX,circleY);

    }

    public void createPlace(Place p,double X,double Y) {
        Circle circle = new Circle(circleX,circleY,10);
        circle.setFill(Color.WHITE);
        circle.setStroke( Color.BLACK );
        circle.setStrokeWidth(1);

        cLabel="P"+circleCounter;
        Text text = new Text(cLabel);
        text.setId(String.valueOf(circleCounter));
        text.setBoundsType(TextBoundsType.VISUAL);
        StackPane stack = new StackPane();
        stack.setId(String.valueOf(circleCounter));

        Translate translate = new Translate();
        translate.setX(X);
        translate.setY(Y);
        stack.getTransforms().add(translate);
        stack.getChildren().addAll(circle,text);
        final Delta dragDelta = new Delta();
        stack.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                if(clickStatus.equals("C"))
                {
                    // record a delta distance for the drag and drop operation.
                    dragDelta.x = stack.getLayoutX() - mouseEvent.getSceneX();
                    dragDelta.y = stack.getLayoutY() - mouseEvent.getSceneY();
                    stack.setCursor(Cursor.MOVE);
                }
                if(clickStatus.equals("D"))
                {
                    circleCounter--;
                    root.getChildren().remove(stack);
                    places.remove(circleCounter-1);
                }
                if(clickStatus.equals("A"))
                {
                    switch (mouseEvent.getButton())
                    {
                        case PRIMARY:
                            vIn = "P"+stack.getId();
                            break;
                        case SECONDARY:
                            vOut = "P"+stack.getId();
                            break;
                        default:
                            break;

                    }
                    if(vIn!="" && vOut!="")
                    {
                        arcs.add(new Arcs(a_count,vIn,vOut));
                        vIn="";vOut="";
                    }
                }
            }
        });
        stack.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                stack.setCursor(Cursor.HAND);
            }
        });
        stack.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                double xx = mouseEvent.getSceneX() + dragDelta.x;
                double yy = mouseEvent.getSceneY() + dragDelta.y;
                stack.setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
                stack.setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
                p.x = xx;
                p.y = yy;
            }
        });

        stack.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        int id = Integer.parseInt(stack.getId())-1;
                        //System.out.println(id);
                        Place p = places.get(id);
                        System.out.println("ID : "+p.id);
                        System.out.println("Label :"+p.label);
                        txt = text;
                        ppp=p;
                        Stage stage1= (Stage) stack.getScene().getWindow();
                        new PlacePropertyDialog(stage1,p, PlacePropertyDialog.ICON_INFO).showAndWait();

                    }
                }
            }
        });

        stacks.add(new Stack("c",stack,null));
        Main.pane = root;
        root.getChildren().add(stack);
        ++circleCounter;
    }

    public void onCtrlZClick() {
        int st = stacks.size();
        undo(st,Main.pane);
    }
    public void onCtrlYClick() {
        int st = tempStacks.size();
        redo(st,Main.pane);
    }

    public void undo(int s, BorderPane pane)
    {
        if(s>0)
        {
            Stack st = Controller.stacks.get(s-1);
            Controller.tempStacks.add(st);
            if(st.type.equals("c"))
            {
                pane.getChildren().remove(st.stk);
                circleCounter--;
            }
            if(st.type.equals("a"))
            {
                pane.getChildren().remove(st.ar);
                a_count--;
            }

            Controller.stacks.remove(s-1);
        }
    }

    public void redo(int t, BorderPane pane)
    {
        if(t>0)
        {
            Stack st = Controller.tempStacks.get(t-1);
            Controller.stacks.add(st);
            if(st.type.equals("c"))
            {
                pane.getChildren().add(st.stk);
                circleCounter++;
            }
            if(st.type.equals("a"))
            {
                pane.getChildren().add(st.ar);
                a_count++;
            }

            Controller.tempStacks.remove(t-1);
        }
    }


    @FXML
    void onTransitionButtonClicked(ActionEvent event) {
        clickStatus="C";
        String l = "T" + rectCounter;
        t_transition.add(new Transition(rectCounter, l, circleX, circleY));
        createTransition();
    }

    private void createTransition() {
        Transition t = t_transition.get(t_transition.size()-1);

        Random random = new Random();
        int max =  random.nextInt(300);
        int min =  random.nextInt(200);
        circleX = Math.random()*(max-min+1)+min;
        circleY = Math.random()*(max-min+1)+min;

        Rectangle rectangle = new Rectangle(200,300,25,30);
        Text text = new Text("T"+rectCounter);
        rectangle.setFill(Color.LIGHTGREY);
        rectangle.setStroke( Color.BLACK );
        rectangle.setStrokeWidth(1);

        text.setBoundsType(TextBoundsType.VISUAL);
        StackPane stack = new StackPane();
        stack.setId(String.valueOf(rectCounter));

        Translate translate = new Translate();
        translate.setX(circleX);
        translate.setY(circleY);
        stack.getTransforms().add(translate);
        stack.getChildren().addAll(rectangle,text);
        final Delta dragDelta = new Delta();
        stack.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                if(clickStatus.equals("C")) {
                    dragDelta.x = stack.getLayoutX() - mouseEvent.getSceneX();
                    dragDelta.y = stack.getLayoutY() - mouseEvent.getSceneY();
                    stack.setCursor(Cursor.MOVE);
                }
                if(clickStatus.equals("D")) {
                    rectCounter--;
                    root.getChildren().remove(stack);
                    t_transition.remove(rectCounter-1);
                }
                if(clickStatus.equals("A")) {
                    switch (mouseEvent.getButton()) {
                        case PRIMARY:
                            System.out.println("t"+stack.getId());
                            vIn = "t"+stack.getId();
                            System.out.println(vIn);
                            break;
                        case SECONDARY:
                            System.out.println(".....o...t"+stack.getId());
                            vOut = "t"+stack.getId();
                            System.out.println(vOut);
                            break;
                        default:
                            break;
                    }
                    if(vIn!="" && vOut!="") {
                        arcs.add(new Arcs(a_count,vIn,vOut));
                        vIn="";vOut="";
                    }
                }
            }
        });
        stack.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                stack.setCursor(Cursor.HAND);
            }
        });
        stack.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                stack.setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
                stack.setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
            }
        });

        stack.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent mouseEvent) {
                double xx = mouseEvent.getSceneX() + dragDelta.x;
                double yy = mouseEvent.getSceneY() + dragDelta.y;
                stack.setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
                stack.setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
                t.x = xx;
                t.y = yy;

            }
        });

        stack.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        rectangle.setFill(Color.GRAY);
                        text.setFill(Color.WHITE);
                    }
                }
            }
        });

        stack.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        int id = Integer.parseInt(stack.getId())-1;
                        //System.out.println(id);
                        Transition t = t_transition.get(id);
                        System.out.println("ID : "+t.id);
                        System.out.println("Label :"+t.label);
                        txt = text;
                        ttt = t;
                        Stage stage1= (Stage) stack.getScene().getWindow();
                        new TransitionPropertyDialog(stage1,t, TransitionPropertyDialog.ICON_INFO).showAndWait();

                    }
                }
            }
        });

        stacks.add(new Stack("c",stack,null));
        root.getChildren().add(stack);
        ++rectCounter;
    }

    public void saveModel()
    {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image FIles(*.png)","*.png");
        fileChooser.getExtensionFilters().add(extensionFilter);
        File file = fileChooser.showSaveDialog(null);
        if(file!=null)
        {
            WritableImage image = Main.scene.snapshot(null);
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image,null),"PNG",file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
     }

    public void openModel() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("TEXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show open file dialog
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            //fileLabel.setText(file.getPath());

            ArrayList<String> list = new ArrayList<String>();
            try
            {
                FileInputStream ff=new FileInputStream(file);
                ObjectInputStream oos=new ObjectInputStream(ff);
                while(true)
                {
                    list=(ArrayList<String>) oos.readObject();
                }
            }
            catch(Exception e)
            {
                System.out.println();
            }
            System.out.println(list.size());
        }
    }

}

class Delta { double x, y; }

class Arcs{
    public int id;
    public String v_in;
    public String v_out;
    public Arcs(int a_count,String start,String end)
    {
        this.id = a_count;
        this.v_in = start;
        this.v_out = end;
    }
}

class Stack {
    public String type;
    public StackPane stk;
    public Arc ar;

    public Stack(String type, StackPane st, Arc ar) {
        this.type = type;
        this.stk = st;
        this.ar = ar;
    }
}
