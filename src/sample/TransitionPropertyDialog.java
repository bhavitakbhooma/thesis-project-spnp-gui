package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class TransitionPropertyDialog extends Stage {
    public static final int ICON_INFO = 0;
    public static final int ICON_ERROR = 1;
    public static Transition t;
    public static Scene scene;

    public TransitionPropertyDialog(Stage owner, Transition t, int type) {
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);

        this.t = t;

        try {
            Parent root= FXMLLoader.load(getClass().getResource("TransitionPropertyDialog.fxml"));
            scene= new Scene(root);
            scene.setFill(Color.WHEAT);
            setScene(scene);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
