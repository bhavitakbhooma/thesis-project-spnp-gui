package sample;

import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PropertyController  implements Initializable {
    @FXML
    TextField name;
    @FXML
    TextField tokens;
    @FXML
    Button save;

    @FXML
    public void CloseDialog() {
        Stage stage = (Stage) save.getScene().getWindow();
        stage.close();
    }

    public void SaveCredentials() {
        Controller.txt.setText(name.getText());
        PlacePropertyDialog.p.label=name.getText();
        Controller.ppp.label=name.getText();
        System.out.println(Controller.ppp.label);
        CloseDialog();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(PlacePropertyDialog.p.label);
        tokens.setText(String.valueOf(PlacePropertyDialog.p.tokens));
    }

}
