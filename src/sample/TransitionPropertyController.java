package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class TransitionPropertyController implements Initializable {

    @FXML
    private Button save;

    @FXML
    private TextField name;

    @FXML
    private TextField tokens;

    @FXML
    public void CloseDialog() {
        Stage stage = (Stage) save.getScene().getWindow();
        stage.close();
    }

    public void SaveCredentials() {
        Controller.txt.setText(name.getText());
        TransitionPropertyDialog.t.label=name.getText();
        Controller.ttt.label=name.getText();
        System.out.println(Controller.ttt.label);
        CloseDialog();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(TransitionPropertyDialog.t.label);
      //  tokens.setText(String.valueOf(TransitionPropertyDialog.t.tokens));
    }

}
