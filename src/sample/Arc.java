package sample;

import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Group;
import javafx.scene.shape.Line;

public class Arc extends Group {

    private final Line line;

    public Arc() {
        this(new Line(), new Line(), new Line());
    }

    private static final double arcLength = 20;
    private static final double arcWidth = 7;

    private Arc(Line line, Line arc1, Line arc2) {
        super(line, arc1, arc2);
        this.line = line;
        InvalidationListener updater = o -> {
            double ex = getEndX();
            double ey = getEndY();
            double sx = getStartX();
            double sy = getStartY();

            arc1.setEndX(ex);
            arc1.setEndY(ey);
            arc2.setEndX(ex);
            arc2.setEndY(ey);

            if (ex == sx && ey == sy) {
                // arrow parts of length 0
                arc1.setStartX(ex);
                arc1.setStartY(ey);
                arc2.setStartX(ex);
                arc2.setStartY(ey);
            } else {
                double factor = arcLength / Math.hypot(sx-ex, sy-ey);
                double factorO = arcWidth / Math.hypot(sx-ex, sy-ey);

                // part in direction of main line
                double dx = (sx - ex) * factor;
                double dy = (sy - ey) * factor;

                // part orthogonal to main line
                double ox = (sx - ex) * factorO;
                double oy = (sy - ey) * factorO;

                arc1.setStartX(ex + dx - oy);
                arc1.setStartY(ey + dy + ox);
                arc2.setStartX(ex + dx + oy);
                arc2.setStartY(ey + dy - ox);
            }
        };

        // add updater to properties
        startXProperty().addListener(updater);
        startYProperty().addListener(updater);
        endXProperty().addListener(updater);
        endYProperty().addListener(updater);
        updater.invalidated(null);
    }

    // start/end properties

    public final void setStartX(double value) {
        line.setStartX(value);
    }

    public final double getStartX() {
        return line.getStartX();
    }

    public final DoubleProperty startXProperty() {
        return line.startXProperty();
    }

    public final void setStartY(double value) {
        line.setStartY(value);
    }

    public final double getStartY() {
        return line.getStartY();
    }

    public final DoubleProperty startYProperty() {
        return line.startYProperty();
    }

    public final void setEndX(double value) {
        line.setEndX(value);
    }

    public final double getEndX() {
        return line.getEndX();
    }

    public final DoubleProperty endXProperty() {
        return line.endXProperty();
    }

    public final void setEndY(double value) {
        line.setEndY(value);
    }

    public final double getEndY() {
        return line.getEndY();
    }

    public final DoubleProperty endYProperty() {
        return line.endYProperty();
    }

}

